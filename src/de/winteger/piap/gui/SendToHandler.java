package de.winteger.piap.gui;

import java.io.IOException;
import java.io.InputStream;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import de.winteger.piap.R;
import de.winteger.piap.consts.PiapIntentConsts;
import de.winteger.piap.core.GlobalSettings;
import de.winteger.piap.data.DataConversion;
import de.winteger.piap.guicontent.CManager;
import de.winteger.piap.helper.DeviceInfo;
import de.winteger.piap.ipc.IProtoMessageParser;
import de.winteger.piap.ipc.IProtoMessageReceiver;
import de.winteger.piap.ipc.IProtoOnConnect;
import de.winteger.piap.ipc.ProtoIPCServer;
import de.winteger.piap.proto.Messages;
import de.winteger.piap.proto.Messages.RootMessage;

public class SendToHandler {
	private String pkg;

	private CheckBox cbSendToLocation = null;
	private CheckBox cbSendToEvals = null;
	private TextView tvSendToLastSyncInput = null;
	private TextView tvSendToLastSyncLocation = null;
	private TextView tvSendToLastSyncEval = null;
	private CheckBox cbSendToInputLogs = null;
	private TextView tvSendToTitle = null;
	private ImageView ivSendToIcon = null;

	private AlertDialog dialog = null;

	private RootMessage rootMessage = null;

	private void initGUI(View root) {
		cbSendToLocation = (CheckBox) root.findViewById(R.id.cbSendToLocation);
		cbSendToEvals = (CheckBox) root.findViewById(R.id.cbSendToEvals);
		tvSendToLastSyncInput = (TextView) root.findViewById(R.id.tvSendToLastSyncInput);
		tvSendToLastSyncLocation = (TextView) root.findViewById(R.id.tvSendToLastSyncLocation);
		tvSendToLastSyncEval = (TextView) root.findViewById(R.id.tvSendToLastSyncEval);
		cbSendToInputLogs = (CheckBox) root.findViewById(R.id.cbSendToInputLogs);
		tvSendToTitle = (TextView) root.findViewById(R.id.tvSendToTitle);
		ivSendToIcon = (ImageView) root.findViewById(R.id.ivSendToIcon);

		ivSendToIcon.setImageResource(DeviceInfo.getAplicationIcon(CManager.CTX, pkg));
	}

	private ProtoIPCServer<RootMessage> ipcServer = null;

	public SendToHandler(String pkg) {
		super();
		this.pkg = pkg;
	}

	public void show() {

		LayoutInflater inflater = LayoutInflater.from(CManager.CTX);
		final View addView;

		addView = inflater.inflate(R.layout.dialog_send_to, null);
		initGUI(addView);

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(CManager.CTX);
		alertDialog.setView(addView);

		alertDialog.setPositiveButton(CManager.CTX.getString(R.string.send), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				startSendTo();
				dialog.dismiss();

			}
		});

		alertDialog.setNegativeButton(CManager.CTX.getString(R.string.cancel), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();

			}
		});

		alertDialog.setTitle("Send to " + DeviceInfo.getAplicationName(CManager.CTX, pkg));

		dialog = alertDialog.create();

		dialog.show();
	}

	protected void startSendTo() {

		if (!DeviceInfo.isInstalled(CManager.CTX, pkg)) {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(CManager.CTX);

			alertDialog.setPositiveButton(CManager.CTX.getString(android.R.string.ok), null);

			alertDialog.setMessage(CManager.CTX.getString(R.string.please_install_the_plugin_) + pkg);
			alertDialog.setTitle(CManager.CTX.getString(R.string.error));
			alertDialog.show();

			return;
		}

		// create data
		RootMessage.Builder b = RootMessage.newBuilder();

		DataConversion dc = new DataConversion();
		if (cbSendToEvals.isChecked()) {
			RootMessage rmMoods = dc.getProtoMoodLogObjects(CManager.CTX);
			b.addAllMoodLog(rmMoods.getMoodLogList());
		}

		if (cbSendToInputLogs.isChecked()) {
			RootMessage rmMoods = dc.getProtoInputLogObjects(CManager.CTX);
			b.addAllInputLog(rmMoods.getInputLogList());
		}

		if (cbSendToLocation.isChecked()) {
			RootMessage rmMoods = dc.getProtoLocationLogObjects(CManager.CTX);
			b.addAllLocationLog(rmMoods.getLocationLogList());
		}

		rootMessage = b.build();

		// create channel
		String socketName = "/SOCK/" + System.currentTimeMillis() + "x";
		ipcServer = new ProtoIPCServer<Messages.RootMessage>(socketName, messageReceiveCallback, messageParserCallback, onConnectCallback);
		ipcServer.connect();

		Intent intent = new Intent();
		intent.putExtra(PiapIntentConsts.INTENT_EXTRA_SOCKET_NAME, socketName);
		intent.setComponent(new ComponentName(GlobalSettings.PIAP_PLUGIN_SENDTOOHMAGE_PKG, "de.winteger.piap.plugin.ohmage.MainActivity"));

		CManager.CTX.startActivity(intent);

	}

	private IProtoMessageReceiver<Messages.RootMessage> messageReceiveCallback = new IProtoMessageReceiver<Messages.RootMessage>() {

		@Override
		public void onReceive(RootMessage mob) {
			// TODO: so far we won't receive anything
		}
	};

	private IProtoMessageParser<Messages.RootMessage> messageParserCallback = new IProtoMessageParser<Messages.RootMessage>() {

		@Override
		public RootMessage parseMessageFromStream(InputStream in) throws IOException {
			return RootMessage.parseDelimitedFrom(in);

		}
	};

	private IProtoOnConnect onConnectCallback = new IProtoOnConnect() {

		@Override
		public void onConnect() {
			try {
				rootMessage.writeDelimitedTo(ipcServer.getOutPutStream());
				ipcServer.getOutPutStream().flush();
				ipcServer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	};
}
