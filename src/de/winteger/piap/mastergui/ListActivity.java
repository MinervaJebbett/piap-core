package de.winteger.piap.mastergui;

import android.content.Intent;
import android.os.Bundle;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import de.winteger.piap.R;
import de.winteger.piap.guicontent.AContentItem;
import de.winteger.piap.guicontent.CManager;

/**
 * An activity representing a list of menu items. This activity has different
 * presentations for handset and tablet-size devices. On handsets, the activity
 * presents a list of items, which when touched, lead to a
 * {@link DetailActivity} representing item details. On tablets, the activity
 * presents the list of items and item details side-by-side using two vertical
 * panes.
 * <p>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link MenuListFragment} and the item details (if present) is a
 * {@link DetailFragment}.
 * <p>
 * This activity also implements the required {@link MenuListFragment.Callbacks}
 * interface to listen for item selections.
 */
public class ListActivity extends SherlockFragmentActivity implements MenuListFragment.Callbacks {

	/**
	 * Whether or not the activity is in two-pane mode, i.e. running on a tablet
	 * device.
	 */
	private boolean mTwoPane;
	private String ITEM_ID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.md_activity_list);

		CManager.CTX = this;
		CManager.ACT = this;
		CManager.initListData();

		if (findViewById(R.id.main_container) != null) {
			// The detail container view will be present only in the
			// large-screen layouts (res/values-large and
			// res/values-sw600dp). If this view is present, then the
			// activity should be in two-pane mode.
			mTwoPane = true;

			// In two-pane mode, list items should be given the
			// 'activated' state when touched.
			((MenuListFragment) getSupportFragmentManager().findFragmentById(R.id.menu_list)).setActivateOnItemClick(true);
		}

		// TODO: If exposing deep links into your app, handle intents here.
	}

	@Override
	protected void onPause() {
		if (mTwoPane) {
			if (ITEM_ID != null) {
				CManager.ITEM_MAP.get(ITEM_ID).onPause();
			}
		}

		super.onPause();
	}

	@Override
	protected void onResume() {
		CManager.CTX = this;
		CManager.ACT = this;

		if (mTwoPane) {
			if (ITEM_ID != null) {
				CManager.ITEM_MAP.get(ITEM_ID).onResume();
			}
		}
		super.onResume();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (mTwoPane) {
			if (ITEM_ID != null) {
				CManager.ITEM_MAP.get(ITEM_ID).onActivityResult(requestCode, resultCode, data);
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * Callback method from {@link MenuListFragment.Callbacks} indicating that
	 * the item with the given ID was selected.
	 */
	@Override
	public void onItemSelected(String id) {
		ITEM_ID = id;

		if (mTwoPane) {
			// In two-pane mode, show the detail view in this activity by
			// adding or replacing the detail fragment using a
			// fragment transaction.
			Bundle arguments = new Bundle();
			arguments.putString(DetailFragment.ARG_ITEM_ID, id);
			DetailFragment fragment = new DetailFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction().replace(R.id.main_container, fragment).commit();

		} else {
			// In single-pane mode, simply start the detail activity
			// for the selected item ID.
			Intent detailIntent = new Intent(this, DetailActivity.class);
			detailIntent.putExtra(DetailFragment.ARG_ITEM_ID, id);
			startActivity(detailIntent);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (CManager.ITEM_MAP.get(ITEM_ID) == null) {
			onItemSelected("idSettings");
			return true;
		}

		return CManager.ITEM_MAP.get(ITEM_ID).onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (mTwoPane) {

			if (ITEM_ID != null) {
				AContentItem mItem = CManager.ITEM_MAP.get(ITEM_ID);
				MenuInflater inflater = getSupportMenuInflater();
				return mItem.onCreateOptionsMenu(CManager.MENU, inflater);
			} else {
				MenuInflater inflater = getSupportMenuInflater();
				inflater.inflate(R.menu.dummy_menu, menu);
				CManager.MENU = menu;
				return true;
			}
		}
		return false;
	}

	@Override
	protected void onDestroy() {
		if (mTwoPane) {
			if (ITEM_ID != null) {
				CManager.ITEM_MAP.get(ITEM_ID).onDestroy();
			}
		}
		super.onDestroy();
	}
}
