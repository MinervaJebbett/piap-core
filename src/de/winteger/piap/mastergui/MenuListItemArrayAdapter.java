package de.winteger.piap.mastergui;

import java.util.ArrayList;
import java.util.List;

import de.winteger.piap.guicontent.AContentItem;
import de.winteger.piap.guicontent.CManager;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class MenuListItemArrayAdapter extends ArrayAdapter<AContentItem> {
	private Context ctx;
	private Resources r;
	private LayoutInflater inflater;
	private ArrayList<AContentItem> listItems;

	public MenuListItemArrayAdapter(Context ctx, LayoutInflater inflater, int textViewResourceId, List<AContentItem> objects) {
		super(ctx, textViewResourceId, objects);
		this.inflater = inflater;
		this.ctx = ctx;
		this.listItems = (ArrayList<AContentItem>) objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return listItems.get(position).onCreateListItemView(inflater, convertView, parent);
	}

	@Override
	public boolean isEnabled(int position) {
		String sid = CManager.ITEMS.get(position).ID;
		if (sid.contains("idListSection")) {
			return false;
		}
		
		return true;
	}
	
	
}