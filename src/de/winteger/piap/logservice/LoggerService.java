package de.winteger.piap.logservice;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import de.winteger.piap.R;
import de.winteger.piap.core.InputLog;
import de.winteger.piap.data.DataSource;
import de.winteger.piap.guicontent.CManager;

/**
 * Service that listens for TYPE_VIEW_TEXT_CHANGED and TYPE_VIEW_CLICKED and
 * stores the input in a DB is always running
 * 
 * @author sarah
 * 
 */
public class LoggerService extends AccessibilityService {

	private final String TAG = "LoggerService";

	private int lastEvent; // stores if the last event was TYPE_VIEW_TEXT_CHANGED or TYPE_VIEW_CLICKED
	private String inputField; // stores the last user input from the input field
	private InputLog lastLog; // stores last user input, app name, app pkg

	private DataSource datasource; // DB access object
	private PackageManager p; // used to get app name + pkg

	/**
	 * is called when the service is started via startService() (is not called
	 * in this way, always stared via onServiceConnected())
	 */
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (CManager.CTX == null) {
			CManager.CTX = this;
		}

		Log.d(TAG, "started");
		onServiceConnected();
		// We want this service to continue running until it is explicitly
		// stopped, so return sticky.
		return START_STICKY;
	}

	/**
	 * is called when the service is automatically started, e.g. after a reboot
	 */
	@Override
	protected void onServiceConnected() {
		// initialize some fields
		setup();

		// Setup the behavior and info for the service
		// maybe more useful to do this once via xml,
		// because this is not changed....

		Log.i(TAG, "onServiceConnected");
		AccessibilityServiceInfo info = new AccessibilityServiceInfo();

		// Set the type of events that this service wants to listen to.  Others
		// won't be passed to this service.
		info.eventTypes = AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED | AccessibilityEvent.TYPE_VIEW_CLICKED;

		// If you only want this service to work with specific applications, set their
		// package names here.  Otherwise, when the service is activated, it will listen
		// to events from all applications.

		// This setting is not used since we want to log the last input if the user clicks on some element
		// in another application (in which logging is not necessarily enabled)

		//info.packageNames = new String[]
		//        {"com.example.android.myFirstApp", "com.example.android.mySecondApp"};

		// Set the type of feedback your service will provide.
		// Feedback is not used for this service
		info.feedbackType = AccessibilityServiceInfo.DEFAULT;

		// FLAG_INCLUDE_NOT_IMPORTANT_VIEWS only for api-version 16 and higher (>= Jelly Bean 4.1)
		info.flags = AccessibilityServiceInfo.FLAG_INCLUDE_NOT_IMPORTANT_VIEWS | AccessibilityServiceInfo.DEFAULT;

		// The timeout after the most recent event
		info.notificationTimeout = 10;

		this.setServiceInfo(info);
	}

	/**
	 * inserts the last user input (with app name + pkg) into DB if logging for
	 * the app is enabled
	 */
	private void logData() {
		// open DB
		datasource.openWrite();
		// check if logging is enabled for this application
		if (datasource.isLoggingEnabled(lastLog.getAppPkg(), lastLog.getAppName())) {
			Log.d(TAG, "Starting DB insert");
			// insert log into DB
			// remove [ and ] from String
			String input = lastLog.getInput().substring(1, lastLog.getInput().length() - 1);
			long id = datasource.createLog(lastLog.getTimestamp(), lastLog.getAppName(), lastLog.getAppPkg(), input);
			Log.d(TAG, "DB insert " + id + " " + lastLog.getInput());
		} else {
			Log.d(TAG, "Not inserted");
		}
		// close DB
		datasource.close();
	}

	/**
	 * is called when an accessibility event is detected (here
	 * TYPE_VIEW_TEXT_CHANGED or TYPE_VIEW_CLICKED)
	 */
	@Override
	public void onAccessibilityEvent(AccessibilityEvent event) {
		int eventType = event.getEventType();
		switch (eventType) {
		case AccessibilityEvent.TYPE_VIEW_CLICKED:
			// we only need to log if we have entered/changed text before
			if (lastEvent == AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED) {
				// assume that we now clicked some view (button, etc) we are done with entering input
				// log the text
				logData();
				// reset the input, we don't want to trigger logging again (see below)
				lastLog.setInput("");
			}
			break;
		case AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED:
			// the text has changed
			// store the current text
			inputField = "" + event.getText();
			// Log.i(TAG, "input: " + inputField);
			// Log.i(TAG, "lastLog.getInput(): " + lastLog.getInput());

			// input field was flushed
			// 2 = "[]" = empty
			if (inputField.length() == 2 && lastLog.getInput() != null && lastLog.getInput().length() > 2) {
				// log old text
				logData();
			}
			// Unfortunately, flushing the input field via enter or delete does not trigger this event :( 
			// (except for api version >= 16)
			// so detect this like this:
			// if the current input is only one symbol and the old input was more than one symbol, log old input
			if (inputField.length() == 3 && lastLog.getInput() != null && lastLog.getInput().length() > 3) { // 3 = "[x]"
				// input was somehow sent without touching another view element and user starts entering text again
				// log old text
				logData();
			}
			// update the values
			lastLog.setTimestamp(System.currentTimeMillis());
			lastLog.setAppName(getApplicationName(event.getPackageName().toString()));
			lastLog.setAppPkg("" + event.getPackageName());
			lastLog.setInput(inputField);
			break;
		default:
			Log.i(TAG, "other: " + event.getEventType());
		}
		// store the current event type		
		lastEvent = eventType;
	}

	@Override
	public void onInterrupt() {
		super.onDestroy();
	}

	/**
	 * initialize variables/objects
	 */
	private void setup() {

		// Initialize logging variables
		lastEvent = -1; // no accessibility event occurred so far
		inputField = ""; // no input occurred so far
		lastLog = new InputLog(0, "", "", ""); // no last user input so far

		// Initialize DB access object
		datasource = new DataSource(this);
		// Initialize packagemanager to get app names + packages
		p = getPackageManager();
	}

	/**
	 * Returns the human-readable application name for the given application
	 * package
	 * 
	 * @param pkg
	 *            the package
	 * @return the name
	 */
	private String getApplicationName(String pkg) {
		ApplicationInfo ainf;
		try {
			ainf = p.getApplicationInfo(pkg, PackageManager.GET_ACTIVITIES);
		} catch (NameNotFoundException e) {
			return getString(R.string._could_not_read_name);
		}
		return _getApplicationName(p, ainf);
	}

	/**
	 * helper function for getApplicationName
	 * 
	 * @param p
	 * @param pkinfo
	 * @return the name
	 */
	private String _getApplicationName(PackageManager p, ApplicationInfo pkinfo) {
		if (pkinfo != null)
			return (String) p.getApplicationLabel(pkinfo);
		return getString(R.string._not_found);
	}
}
