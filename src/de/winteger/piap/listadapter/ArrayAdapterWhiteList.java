package de.winteger.piap.listadapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import de.winteger.piap.R;
import de.winteger.piap.core.AppListItem;

/**
 * This class defines how AppListItems are displayed in a ListView and updates the ListView
 * @author sarah
 *
 */
public class ArrayAdapterWhiteList extends ArrayAdapter<AppListItem> {
	
	private final String TAG = "Logger";	
	private LayoutInflater inflater; // Obtains the layout
	private ArrayList<AppListItem> entryList; // List of AppListItems that is displayed in a ListView

	/**
	 * Creates new ArrayAdapterWhiteList with the given parameters
	 * @param context
	 * @param inflater
	 * @param textViewResourceId
	 * @param objects
	 */
	public ArrayAdapterWhiteList(Context context, LayoutInflater inflater, int textViewResourceId, List<AppListItem> objects) {
		super(context, textViewResourceId, objects);
		this.inflater = inflater;
		this.entryList = (ArrayList<AppListItem>) objects;
	}

	/**
	 * sets the entryList to the list list 
	 * @param list the list
	 */
	public void updateEntryList(ArrayList<AppListItem> list) {
		entryList = list;
		Log.i(TAG, "List size: " + list.size());
	}

	/**
	 * defines how an item of the list view looks like
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// get the layout for a list item
		View entry = inflater.inflate(R.layout.lv_app_select_item, parent, false);

		TextView tvApp = (TextView) entry.findViewById(R.id.tvAppName);
		CheckBox cb = (CheckBox) entry.findViewById(R.id.cBLog);

		// get the AppListItem
		AppListItem item = entryList.get(position);

		// get data from the AppListItem
		String name = item.getAppName() != null ? item.getAppName() : getContext().getString(R.string._empty_name);
		boolean logEnabled = item.isLogEnabled();

		// fill layout with the data
		tvApp.setText(name);
		cb.setChecked(logEnabled);
		return entry;
	}
}