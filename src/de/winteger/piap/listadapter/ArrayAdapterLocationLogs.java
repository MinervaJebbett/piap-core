package de.winteger.piap.listadapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import de.winteger.piap.R;
import de.winteger.piap.core.LocationLog;
import de.winteger.piap.helper.TimeHelper;

/**
 * This class defines how LocationLogs are displayed in a ListView and updates
 * the ListView
 * 
 * @author sarah
 * 
 */
public class ArrayAdapterLocationLogs extends ArrayAdapter<LocationLog> {

	private final String TAG = "Logger";
	private Context context; // Context that contains the ListView
	private LayoutInflater inflater; // Obtains the layout
	private ArrayList<LocationLog> entryList; // List of InputLogs that is displayed in a ListView

	/**
	 * Creates new ArrayAdapterInputLogs with the given parameters
	 * 
	 * @param context
	 * @param inflater
	 * @param textViewResourceId
	 * @param objects
	 */
	public ArrayAdapterLocationLogs(Context context, LayoutInflater inflater, int textViewResourceId, List<LocationLog> objects) {
		super(context, textViewResourceId, objects);
		this.inflater = inflater;
		this.context = context;
		this.entryList = (ArrayList<LocationLog>) objects;
	}

	/**
	 * sets the entryList to the list list
	 * 
	 * @param list
	 *            the list
	 */
	public void updateEntryList(ArrayList<LocationLog> list) {
		entryList = list;
		Log.i(TAG, "List size: " + list.size());
	}

	/**
	 * defines how an item of the list view looks like
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// get the layout for a list item
		View entry = inflater.inflate(R.layout.lv_default_item, parent, false);

		// to lazy to do another layout for location logs --> reuse layout for text logs
		TextView tvTime = (TextView) entry.findViewById(R.id.tvTime);
		TextView tvEntering = (TextView) entry.findViewById(R.id.tvName);
		TextView tvInfo = (TextView) entry.findViewById(R.id.tvInput);

		// get the location log
		LocationLog item = entryList.get(position);

		// get data from the input log
		boolean entering = item.isEntering();
		String time = item.getTimestamp() != 0 ? TimeHelper.getDateTime(context, item.getTimestamp()) : getContext().getString(R.string._no_time);

		if (entering) {
			time = "returned on " + time;
		} else {
			time = "left on " + time;
		}

		// Try to get the time from previous input log...
		LocationLog itemBefore = null;
		LocationLog itemNext = null;
		LocationLog prev = null;
		long timestamp = item.getTimestamp();

		// What is the previous log? (dependent on current sorting..)
		if (position > 0)
			itemBefore = entryList.get(position - 1);
		if (position < entryList.size() - 1)
			itemNext = entryList.get(position + 1);

		if (itemBefore != null && timestamp - itemBefore.getTimestamp() >= 0) {
			prev = itemBefore;
		} else if (itemNext != null && timestamp - itemNext.getTimestamp() >= 0) {
			prev = itemNext;
		}

		// Compute time at home/away
		String input = "";
		if (prev != null) {
			if (prev.isEntering() != item.isEntering()) {
				long prevTime = prev.getTimestamp();
				long timediff = item.getTimestamp() - prevTime;
				if (entering) {
					input = "Away for " + TimeHelper.getTimeString(timediff);
				} else {
					input = "At home for " + TimeHelper.getTimeString(timediff);
				}
			}
		}

		// fill the layout with the data
		tvTime.setText(input);
		tvEntering.setText("");
		tvInfo.setText(time);

		if (entering) {
			//entry.setBackgroundColor(Color.argb(5, 0, 255, 0));
		} else {
			entry.setBackgroundColor(Color.argb(5, 255, 0, 0));
		}

		return entry;
	}
}