package de.winteger.piap.guicontent;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import de.winteger.piap.R;
import de.winteger.piap.core.EvalAccess;
import de.winteger.piap.core.InputLog;
import de.winteger.piap.evaluation.Evaluation;
import de.winteger.piap.listadapter.ArrayAdapterInputLogsEvaluation;

/**
 * Activity to view the evaluation
 * 
 * @author sarah
 * 
 */
public class ContentEvaluation extends AContentItem {

	public ContentEvaluation(String id, String title, int iconID) {
		super(id, title, iconID);
	}

	private final String TAG = "Logger";
	private ListView relevantLogsListView; // shows the relevant logs
	private ArrayList<InputLog> relavantLogsEntryList; // stores the relevant logs
	private ArrayAdapterInputLogsEvaluation logsArrayAdapter; // notifies the log view about changes
	private TextView tvEvalResult; // tv to display the human-readable result of the evaluation 
	private Evaluation evaluation; // contains the results of the last evaluation
	private int[] colorArray; // colors for the legend of categories
	private String[] categoryDescArray; // descriptions for the legend of categories

	private Context ctx;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ctx = CManager.CTX;
		View rootView = inflater.inflate(R.layout.activity_evaluation, container, false);

		colorArray = EvalAccess.getCategoryColors(ctx);
		categoryDescArray = EvalAccess.getCategoryDescriptors(ctx);

		tvEvalResult = (TextView) rootView.findViewById(R.id.tvEvalResult);

		relevantLogsListView = (ListView) rootView.findViewById(R.id.lvRelevantLogs);
		relavantLogsEntryList = new ArrayList<InputLog>();

		// load last evaluation
		if ((evaluation = EvalAccess.loadLastEvaluation(ctx)) == null) {
			// loading evaluation failed
			evaluation = new Evaluation(ctx);
		}

		LinearLayout ll = (LinearLayout) rootView.findViewById(R.id.llCategoryLegend);
		// set up the legend
		for (int i = 1; i < 25; i++) {
			TextView t = new TextView(ctx);
			t.setMinEms(1);
			t.setMaxEms(1);
			t.setGravity(Gravity.LEFT);
			t.setPadding(8, 0, 0, 0);
			// set BG
			if (colorArray.length > i - 1) {
				t.setBackgroundColor(colorArray[i - 1]);
			}
			t.setTextColor(Color.WHITE);
			// set category number + description
			if (categoryDescArray.length > i - 1) {
				t.setText("" + i + " " + categoryDescArray[i - 1]);
			} else {
				t.setText("" + i);
			}
			ll.addView(t);
		}
		LayoutInflater myInflater = (LayoutInflater) CManager.CTX.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		// set up data
		relavantLogsEntryList = evaluation.getRelevantLogs();
		logsArrayAdapter = new ArrayAdapterInputLogsEvaluation(ctx, myInflater, R.id.lvRelevantLogs, relavantLogsEntryList);
		relevantLogsListView.setAdapter(logsArrayAdapter);
		tvEvalResult.setText(evaluation.getInfo());
		return rootView;
	}
}
