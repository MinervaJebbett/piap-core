package de.winteger.piap.guicontent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;

import com.actionbarsherlock.view.Menu;

import de.winteger.piap.R;
import de.winteger.piap.core.GlobalSettings;
import de.winteger.piap.core.Plugin;
import de.winteger.piap.helper.DeviceInfo;

public class CManager {

	public static Context CTX;
	public static Activity ACT;

	/**
	 * An array of sample (dummy) items.
	 */
	public static List<AContentItem> ITEMS = new ArrayList<AContentItem>();

	/**
	 * A map of sample (dummy) items, by ID.
	 */
	public static Map<String, AContentItem> ITEM_MAP = new HashMap<String, AContentItem>();
	public static Menu MENU;

	public static void initListData() {
		ITEMS.clear();
		ITEM_MAP.clear();

		/*
		 * Add your [extends AContentItem] classes here The parameters are "id",
		 * "Title" <- shown in the list view
		 */

		addItem(new ContentListSectionItem("idListSection0", "Data"));
		addItem(new ContentInputLogs("idInputLogs", "Input Logs", R.drawable.ic_md_inputlog));
		if (DeviceInfo.isInstalled(CTX, GlobalSettings.PIAP_PLUGIN_LOCATION_PKG)) {
			addItem(new ContentLocationLogs("idLocationLogs", "Location Logs", R.drawable.ic_md_locationlog));
		}

		addItem(new ContentListSectionItem("idListSection1", "Evaluation"));
		addItem(new ContentEvaluation("idEvaluation", "Evaluation", R.drawable.ic_md_evaluation));
		addItem(new ContentInputEvalChart("idInputChart", "Evaluation Chart", R.drawable.ic_md_evalchart));
		if (DeviceInfo.isInstalled(CTX, GlobalSettings.PIAP_PLUGIN_LOCATION_PKG)) {
			addItem(new ContentLocationChart("idLocationChart", "Location Chart", R.drawable.ic_md_locationchart));

		}

		addItem(new ContentListSectionItem("idListSection2", "Actions"));
		addItem(new ContentAction("idActionEvaluate", "Evaluate Now", R.drawable.ic_md_evalnow));
		if (DeviceInfo.isInstalled(CTX, GlobalSettings.PIAP_PLUGIN_SENDTOOHMAGE_PKG)) {
			addItem(new ContentAction("idActionSendToOhmage", "Send to Ohmage", R.drawable.ic_ohmage));
		}
		if (DeviceInfo.isInstalled(CTX, GlobalSettings.PIAP_PLUGIN_SENDTODEVICE_PKG)) {
			addItem(new ContentAction("idActionSendToDevice", "Exchange with Device", R.drawable.ic_md_sendtodevice));
		}

		addItem(new ContentListSectionItem("idListSection3", "Application"));
		addItem(new ContentPlugins("idPlugins", "Plugins", R.drawable.ic_md_plugins));
		addItem(new ContentSettings("idSettings", "Settings", R.drawable.ic_md_settings));
		addItem(new ContentAbout("idAbout", "About", R.drawable.ic_md_about));
		// about	

		if (GlobalSettings.isDemo()) {
			addItem(new ContentDemoMode("idDemoMode", "Demo Stuff"));
		}

	}

	public static List<Plugin> getPlugins() {
		List<Plugin> pluginList = new ArrayList<Plugin>();

		pluginList
				.add(new Plugin(R.drawable.ic_launcher, CManager.CTX.getString(R.string.piap_location_logger), GlobalSettings.PIAP_PLUGIN_LOCATION_PKG));
		pluginList.add(new Plugin(R.drawable.ic_launcher, CManager.CTX.getString(R.string.piap_sms_notifier), GlobalSettings.PIAP_PLUGIN_NOTIFIER_PKG));
		pluginList.add(new Plugin(R.drawable.ic_launcher, CManager.CTX.getString(R.string.piap_omage_integration),
				GlobalSettings.PIAP_PLUGIN_SENDTOOHMAGE_PKG));
		pluginList.add(new Plugin(R.drawable.ic_ohmage, CManager.CTX.getString(R.string.ohmage_client_application),
				GlobalSettings.PIAP_APP_OHMAGE_CLIENT_PKG));
		return pluginList;
	}
	
	private static void addItem(AContentItem item) {
		ITEMS.add(item);
		ITEM_MAP.put(item.ID, item);
	}

}
