package de.winteger.piap.core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;

import android.content.Context;
import android.util.Log;
import de.winteger.piap.R;
import de.winteger.piap.evaluation.Evaluation;

public class EvalAccess {

	static final String TAG = "EvalAccess";

	public static String[] getCategoryDescriptors(Context context) {
		String[] desc = { context.getString(R.string.depressed_mood), context.getString(R.string.loss_of_interest),
				context.getString(R.string.significant_change_in_appetite), context.getString(R.string.sleep_problems),
				context.getString(R.string.psychomotor_agitation), context.getString(R.string.psychomotor_retardation),
				context.getString(R.string.loss_of_energy), context.getString(R.string.feelings_of_guilt_worthlessness_negative_self_appraisal),
				context.getString(R.string.diminished_concentration_or_indecisiveness), context.getString(R.string.suicidal_thoughts),
				context.getString(R.string.negative_view_of_the_self), context.getString(R.string.negative_view_of_the_world_or_life_experiences),
				context.getString(R.string.negative_view_of_the_future), context.getString(R.string.irrational_beliefs),
				context.getString(R.string.emotional_deprivation), context.getString(R.string.abandonment_or_instability),
				context.getString(R.string.mistrust_or_expectation_of_abuse), context.getString(R.string.social_isolation),
				context.getString(R.string.feelings_of_defectiveness_or_shame), context.getString(R.string.feelings_of_inferiority),
				context.getString(R.string.feelings_of_failure), context.getString(R.string.feelings_of_dependence_or_incompetence),
				context.getString(R.string.feelings_of_vulnerability_to_harm_or_illness), context.getString(R.string.insufficient_control_or_self_discipline) };
		return desc;
	}

	public static int[] getCategoryColors(Context context) {
		int[] colors = { context.getResources().getColor(R.color.red), context.getResources().getColor(R.color.darkgray),
				context.getResources().getColor(R.color.blue), context.getResources().getColor(R.color.fuchsia), context.getResources().getColor(R.color.gray),
				context.getResources().getColor(R.color.green), context.getResources().getColor(R.color.lime), context.getResources().getColor(R.color.maroon),
				context.getResources().getColor(R.color.navy), context.getResources().getColor(R.color.olive), context.getResources().getColor(R.color.purple),
				context.getResources().getColor(R.color.DarkOrange), context.getResources().getColor(R.color.silver),
				context.getResources().getColor(R.color.teal), context.getResources().getColor(R.color.HotPink),
				context.getResources().getColor(R.color.DeepSkyBlue), context.getResources().getColor(R.color.DarkGreen),
				context.getResources().getColor(R.color.DarkSlateGray), context.getResources().getColor(R.color.DarkGoldenRod),
				context.getResources().getColor(R.color.RosyBrown), context.getResources().getColor(R.color.LimeGreen),
				context.getResources().getColor(R.color.Snow4), context.getResources().getColor(R.color.DarkTurquiose),
				context.getResources().getColor(R.color.SteelBlue) };
		return colors;
	}

	/**
	 * Loads the last computed evaluation
	 * 
	 * @param context
	 *            The context
	 * @return The evaluation
	 */
	public static Evaluation loadLastEvaluation(Context ctx) {
		String filePath = "lastEval.eval";
		FileInputStream fis;
		Evaluation temp = null;
		Log.d(TAG, "Try to load last Evaluation");
		try {
			fis = ctx.openFileInput(filePath);
			ObjectInputStream obj_in = new ObjectInputStream(fis);
			temp = (Evaluation) obj_in.readObject();
			fis.close();
		} catch (FileNotFoundException e) {
			Log.d(TAG, "file not found " + e.getMessage());
			return null;
		} catch (StreamCorruptedException e) {
			Log.d(TAG, "stream corrupted " + e.getMessage());
			return null;
		} catch (IOException e) {
			Log.d(TAG, "ioexption " + e.getMessage());
			return null;
		} catch (ClassNotFoundException e) {
			Log.d(TAG, "class not found " + e.getMessage());
			return null;
		}
		Log.d(TAG, "Loaded last evaluation");
		return temp;
	}

	/**
	 * Saves the evaluation
	 * 
	 * @return 0 if save was successful, -1 else
	 */
	public static int saveEvaluation(Context context, Evaluation evaluation) {
		try {
			FileOutputStream fos = context.openFileOutput("lastEval.eval", Context.MODE_PRIVATE);
			ObjectOutputStream obj_out = new ObjectOutputStream(fos);
			obj_out.writeObject(evaluation);
			fos.close();
			Log.d(TAG, "Saved evaluation");
		} catch (FileNotFoundException e) {
			Log.d(TAG, "Save evaluation fail, filenotfound " + e.getMessage());
			return -1;
		} catch (IOException e) {
			Log.d(TAG, "Save evaluation fail " + e.getMessage());
			return -1;
		}
		return 0;
	}

}
