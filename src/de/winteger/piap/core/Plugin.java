package de.winteger.piap.core;

public class Plugin {
	private int mIconID;
	private String mName;
	private String mPackageName;

	public Plugin(int iconID, String name, String packageName) {
		super();
		this.mIconID = iconID;
		this.mName = name;
		this.mPackageName = packageName;
	}

	public int getIconID() {
		return mIconID;
	}

	public void setIconID(int iconID) {
		this.mIconID = iconID;
	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		this.mName = name;
	}

	public String getPackageName() {
		return mPackageName;
	}

	public void setPackageName(String packageName) {
		this.mPackageName = packageName;
	}
}
