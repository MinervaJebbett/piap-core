package de.winteger.piap.core;

import java.io.Serializable;

/**
 * Contains information about a user input (input, time, app name, app package)
 * 
 * @author sarah
 * 
 */
public class InputLog implements Serializable {
	private static final long serialVersionUID = -995906992742151092L;
	private long timestamp;
	private String appName;
	private String appPkg;
	private String input;
	
	// These attributes are currently only used in the evaluation
	// values are not stored in the DB, so the values might be wrong if not explicitly set
	private boolean[] categories = new boolean[25]; // Categories[x] = true if keyword from category x occurred
	private int[] logsInCategories = new int[25]; // logsInCategories[x] = y if y keyword from category x occurred
	private int numberCategories = 0;
	private int numberKeywords = 0;

	public InputLog() {
		super();
	}

	public InputLog(long timestamp, String appName, String appPkg, String input) {
		super();
		this.timestamp = timestamp;
		this.appName = appName;
		this.appPkg = appPkg;
		this.input = input;
	}

	/**
	 * Two InputLogs are equal if timestamp, appName, appPkg and input are equal
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		InputLog other = (InputLog) obj;
		if (appName == null) {
			if (other.appName != null) {
				return false;
			}
		} else if (!appName.equals(other.appName)) {
			return false;
		}
		if (appPkg == null) {
			if (other.appPkg != null) {
				return false;
			}
		} else if (!appPkg.equals(other.appPkg)) {
			return false;
		}
		if (input == null) {
			if (other.input != null) {
				return false;
			}
		} else if (!input.equals(other.input)) {
			return false;
		}
		if (timestamp != other.timestamp) {
			return false;
		}
		return true;
	}

	public int getNumberCategories() {
		return numberCategories;
	}

	public void setNumberCategories(int numberCategories) {
		this.numberCategories = numberCategories;
	}

	public int getNumberKeywords() {
		return numberKeywords;
	}

	public void setNumberKeywords(int numberKeywords) {
		this.numberKeywords = numberKeywords;
	}
	
	public void setCategoryStatus(int category) {
		if (category > 0 && category < 25) {
			categories[category] = true;		
		}
	}
	
	public boolean getCategoryStatus(int category) {
		if (category > 0 && category < 25) {
			return categories[category];		
		}
		return false;
	}
	
	public void incrementCategoryCounter(int category) {
		if (category > 0 && category < 25) {
			logsInCategories[category]++;
		}
	}

	public int getCategoryCounter(int category) {
		if (category > 0 && category < 25) {
			return logsInCategories[category];
		}
		return -1;
	}
	
	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppPkg() {
		return appPkg;
	}

	public void setAppPkg(String appPkg) {
		this.appPkg = appPkg;
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}
}
